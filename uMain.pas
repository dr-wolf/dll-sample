unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, uDLLHeader;

type
  TfrmMain = class(TForm)
    bRun: TButton;
    eByte: TEdit;
    lByte: TLabel;
    lCardinal: TLabel;
    eCardinal: TEdit;
    procedure bRunClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.bRunClick(Sender: TObject);
var
  p: TParamSet;
  s: string;
  n: Integer;
begin
  p.ByteExample := StrToIntDef(eByte.Text, 0);
  p.CardinalExample := StrToIntDef(eCardinal.Text, 0);

  if FuncSample(@p) = S_OK then
  begin
    eByte.Text := IntToStr(p.ByteExample);
    eCardinal.Text := IntToStr(p.CardinalExample);

    s := IntToStr(p.ArrayExample[0]);
    for n := 1 to 7 do
      s := s + ', ' + IntToStr(p.ArrayExample[n]);

    ShowMessage('[' + s + ']');
  end;
end;

end.
