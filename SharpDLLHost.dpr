program SharpDLLHost;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uDLLHeader in 'uDLLHeader.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
