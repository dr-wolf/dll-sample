unit uDLLHeader;

interface

type
  TParamSet = packed record
    ByteExample: Byte;
    CardinalExample: Cardinal;
    ArrayExample: array [0 .. 7] of Byte;
  end;

  PParamSet = ^TParamSet;

function FuncSample(const ParamSet: PParamSet): HRESULT; stdcall;  external 'SharpDLL.dll';

implementation

end.
