object frmMain: TfrmMain
  Left = 789
  Top = 451
  BorderStyle = bsDialog
  Caption = 'DLLHost'
  ClientHeight = 152
  ClientWidth = 139
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lByte: TLabel
    Left = 8
    Top = 8
    Width = 22
    Height = 13
    Caption = 'Byte'
  end
  object lCardinal: TLabel
    Left = 8
    Top = 54
    Width = 39
    Height = 13
    Caption = 'Cardinal'
  end
  object bRun: TButton
    Left = 32
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 0
    OnClick = bRunClick
  end
  object eByte: TEdit
    Left = 8
    Top = 27
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 1
    Text = '16'
  end
  object eCardinal: TEdit
    Left = 8
    Top = 73
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 2
    Text = '64'
  end
end
