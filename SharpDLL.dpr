library SharpDLL;

uses
  SysUtils,
  Classes,
  uDLLHeader in 'uDLLHeader.pas';

function FuncSample(const ParamSet: PParamSet): HRESULT; stdcall;
var
  n: Integer;
begin
  ParamSet.CardinalExample := ParamSet.ByteExample * ParamSet.ByteExample;
  for n := 0 to 7 do
    ParamSet.ArrayExample[n] := ParamSet.ByteExample + n;
  Result := S_OK;
end;

exports
   FuncSample;

{$R *.res}

begin
end.
